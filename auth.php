<?php

define('STATUS_WRONG_PASSWORD', 0);
define('STATUS_OK', 1);
define('STATUS_NO_SUCH_USER', 2);
define('STATUS_ACCOUNT_BLOCKED', 3);

$dns = 'mysql:host=sql102.byethost15.com;dbname=b15_13887579_webLogin';
$user = 'b15_13887579';
$passwd= "";
if (isset($_GET['dbpass']) {
	$passwd = $_GET['dbpass'];
} else {
	echo "You need to give the db password";
	return 0;
}
$connection = new PDO( $dns, $user, $passwd );

session_start();
// If the user already has a session, then he does not need to log in.
if (isset($_SESSION['login'])) {
    $login = $_SESSION['login'];
    echo "Hello $login, you don't need to connect.";
} else {
    if (isset($_GET['login']) && isset($_GET['passwd'])) {
        $userLogin = $_GET['login'];
        $userPasswd = $_GET['passwd'];

        $query = "SELECT passwd, wrong_tries FROM user WHERE login = '$userLogin'";
        $select = $connection->query($query);
        
        if ($select) {
            if ($select->rowCount() == 1) {
                $rowFetched = $select->fetch();
                $savedPasswd = $rowFetched['passwd'];
                $wrongTries = $rowFetched['wrong_tries'];
                $savedSalt = substr($savedPasswd, -5);
                $encryptedUserTry = md5($userPasswd . $savedSalt) . $savedSalt;
                
                if ($wrongTries >= 3) {
                    $resultTab["status"] = STATUS_ACCOUNT_BLOCKED;
                    echo json_encode($resultTab);
//                    echo "Account blocked. Sorry, you have to contact an admin. And I am not giving you any email adress.";
                } else {
                    if ($savedPasswd == $encryptedUserTry) {
                        $_SESSION['login'] = $userLogin;
                        $resultTab["status"] = STATUS_OK;
                        echo json_encode($resultTab);
                    } else {
                        $resultTab["status"] = STATUS_WRONG_PASSWORD;
                        $resultTab["triesLeft"] = (3-$wrongTries);
//                        echo "Wrong password ! Only ".(3-$wrongTries)." tries left";
                        
                        $newWrongTriesValue=$wrongTries+1;
                        $updateQuery = "UPDATE user SET wrong_tries=$newWrongTriesValue WHERE login='$userLogin'";
                        try {
                            $connection->query($updateQuery);
                        } catch (Exception $e) {
                            echo "error updating wrong tries: " . $e->getMessage();
                            echo "N° " . $e->getCode();
                        }
                        echo json_encode($resultTab);
                    }
                }
            }
            else {
                $resultTab["status"] = STATUS_NO_SUCH_USER; 
                echo json_encode($resultTab);
            }
        } else {
            echo("error !");
        }
    }
    else {
        echo("you need to give a login and a password");
    }
}
?>
